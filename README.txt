 _                           
| |__   ___   __ _ _   _ ___ 
| '_ \ / _ \ / _` | | | / __|
| |_) | (_) | (_| | |_| \__ \
|_.__/ \___/ \__, |\__,_|___/
             |___/

Bogus is a stack-based esoteric programming language designed to
see how hard it can to program without being able to explicitly
set values.

instruction set
---------------

d  - dup  (a -- a a)
s  - swap (a b -- b a)
r  - rot  (a b c -- b c a)
o  - over (a b -- a b a)
y  - yeet (a -- )
R  - generate a random positive number
+  - add  (a b -- c)
-  - sub  (a b -- c)
%  - bitwise AND with 255
() - codeblock
; - comment
[A-Za-z0-9] - function
?  - if >0 then (codeblock)
!  - while >0 (codeblock)
~  - logical NOT
&  - logical AND
|  - logical OR
.  - print
,  - input
>  - push from main stack to stack B
<  - push from stack B to main stack
:  - dup from stack B to main stack
/  - true when main stack is empty
\  - true when stack B is empty
*  - halt the program
`  - debugger

usage
-----

 $ bogus [[-d] FILENAME]

 * the -d flag enables debug mode.
 If no argument has been provided, it will go in repl mode.

codeblocks
----------

A codeblock is a piece of code between parantheses that allows to do
flow control and functions. Even thought this language's syntax and
instructions are influenced by FALSE to some extent, it does not work
like FALSE's lambda as it would break one of the programming standards.

A codeblock must be behind a flow-control or function instruction.
Example: ?(...) f(...)

If a function instruction is used without a codeblock behind, it will
run the code of the codeblock.

i/o
---

There is a print command, and will pop the top value off the main stack,
bitwise AND it with 255 and display it as a character.

There is also an input command, but to keep the random aspect of the
interpreter and respect standards, an 8-bit constant called "spice"
will be randomly generated and will add with the user input (which is
an ascii character). There is no way to get this number in the
interpreter, it is only kept internally.

debug
-----

The interpreter has debugger functions that can be triggered if the
debug mode is enabled.
NOTE: For people who programs their own Bogus implementation, the
debugger does not have to be implemented, it's just a little feature I
implemented to make programming easier.

 * `b - Breakpoint. Will give you the seed, the spice, where it
   stopped and you will be able to run a line in REPL mode. That means
   either repl commands (see repl) or arbitrary code.
 * `d - Prints the top value on the main stack
 * `e - Prints the top value on stack B
 * `p - Prints the truthiness of the top value on the main stack
 * `q - Prints the truthiness of the top value on stack B
 * `i - Waits for input and then discards it

repl
----

Contains two commands
 :exit - exits
 :stack - displays the top 10 elements of the main stack
 :stackb - displays the top 10 elements of stack B

about logic
-----------

It would be expected that true is 1 and false is 0.
However it would make programming too easy, so when false, a random non-positive
number will be generated but if true, a random positive number will be
generated.

programming standards
---------------------

There is some very simple standards/rules to follow when implementing
an interpreter.

 * The interpreter must lack of any means of getting a consistent
   value using explicit/obvious instructions (example: literals or
   implementing division).
 * Since Bogus is not intended to be used with specific random seed,
   any random number generator can be used to implement a Bogus
   interpreter.
 * Programs written in Bogus must work with (almost) any random seed,
   no matter what the random number generator is.
 * Programs should have an "accuracy score". This is a score scaled in
   percentage. See the next part.
 * Programs must not rely on debugging features.

The standard does not have to be respected. However a program that
does not conform to these will be considered "non-standard". (Example:
implementation specific programs that messes with a specific random
number generator or implementation)

accuracy score
--------------

To make an accuracy score, you have to run your Bogus program with different
seeds. If your program returns the expected result, the seed is compatible
with your program, otherwise it isn't.

The score is the percentage of compatible seeds. The more seed the
programmer, the more accurate the accuracy score will be. The goal of the
programmer is to maximize the accuracy score of their program.

Relying on external sources for data (example: user input) disqualifies
your program from having an accuracy score.

why is it called bogus
----------------------

because of bogo sort.
bogo sort is basically a very unefficient sorting algorithm that
randomly shuffles the concerned array until it's sorted.
