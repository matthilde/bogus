/*
 * Bogus programming language
 */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <readline/readline.h>
#include <stdint.h>

typedef int32_t I;

#define STACK_SIZE 1024
#define die(x) { perror(x); exit(1); }
#define pdie(x) { fprintf(stderr, "%s\n", x); exit(1); }
#define fdie(x,y) { fprintf(stderr, "at %d: %s\n",x,y); exit(1); }
#define strequ(x,y) (strcmp(x,y) == 0)

I stack[STACK_SIZE];
I stackb[STACK_SIZE];
int32_t stackptr;
int32_t stackbptr;

uint8_t spice;
size_t functions[256] = { 0 };
unsigned long bogus_seed;
bool debug_mode = false;

void bogus_init() {
	spice = rand();
	stackptr = stackbptr = 0;
}
void bogus_push(I n) {
	stack[stackptr++] = n;
	if (stackptr >= STACK_SIZE)
		pdie("Stack overflow.");
}
I bogus_pop() {
	if (--stackptr < 0)
		pdie("Stack underflow.");
	return stack[stackptr];
}
void bogus_b_push(I n) {
	stackb[stackbptr++] = n;
	if (stackbptr >= STACK_SIZE)
		pdie("Stack B overflow.");
}
I bogus_b_pop() {
	if (--stackbptr < 0)
		pdie("Stack B underflow.");
	return stackb[stackbptr];
}
I bogus_bool(bool b) {
	if (b)
		return (rand()%RAND_MAX)+1;
	else
		return 0 - rand();
}

size_t bogus_do_codeblock(const char*, size_t, size_t);
size_t bogus_skip_codeblock(const char* prgm, size_t pc, size_t prgm_len);
bool   bogus_prompt_one();

void bogus_eval_one(const char* prgm, size_t* pc, size_t prgm_len) {
	I a,b,c;

	char is = prgm[*pc];
	switch (is) {
	case 'd':
		a = bogus_pop();
		bogus_push(a); bogus_push(a);
		break;
	case 's':
		a = bogus_pop();
		b = bogus_pop();
		bogus_push(a);
		bogus_push(b);
		break;
	case 'r':
		c = bogus_pop();
		b = bogus_pop();
		a = bogus_pop();
		bogus_push(b);
		bogus_push(c);
		bogus_push(a);
		break;
	case 'o':
		a = bogus_pop();
		b = bogus_pop();

		bogus_push(b);
		bogus_push(a);
		bogus_push(b);
		break;
	case 'y': bogus_pop(); break;
	case 'R': bogus_push((rand()%RAND_MAX)+1); break;
	case '+':
		bogus_push(bogus_pop() + bogus_pop());
		break;
	case '-':
		a = bogus_pop();
		bogus_push(bogus_pop() - a);
		break;
	case '%':
		bogus_push(bogus_pop() & 0xff);
		break;
	case ';':
		for (++(*pc); prgm[*pc] != '\n' && *pc < prgm_len; ++(*pc));
		break;
	case '?':
		if (bogus_pop() > 0)
			*pc = bogus_do_codeblock(prgm, *pc+1, prgm_len);
		else
			*pc = bogus_skip_codeblock(prgm, *pc+1, prgm_len);
		break;
	case '!':
		a = bogus_pop();
		while (a > 0) {
			bogus_do_codeblock(prgm, *pc+1, prgm_len);
			a = bogus_pop();
		}
		*pc = bogus_skip_codeblock(prgm, *pc+1, prgm_len);
		break;
	case '~':
		bogus_push(bogus_bool(!(bogus_pop() > 0)));
		break;
	case '&':
		a = bogus_bool(bogus_pop() > 0 && bogus_pop() > 0);
		bogus_push(a);
		break;
	case '|':
		a = bogus_bool(bogus_pop() > 0 || bogus_pop() > 0);
		bogus_push(a);
		break;
	case '.':
		a = bogus_pop() & 0xff;
		putchar(a);
		break;
	case ',':
		a = getchar() + spice;
		bogus_push(a);
		break;

	case '>':
		bogus_b_push(bogus_pop());
		break;
	case '<':
		bogus_push(bogus_b_pop());
		break;
	case ':':
		a = bogus_b_pop();
		bogus_b_push(a);
		bogus_push(a);
		break;
	case '/':
		bogus_push(bogus_bool(stackptr == 0));
		break;
	case '\\':
		bogus_push(bogus_bool(stackbptr == 0));
		break;
	case '*':
		exit(0);
		break;

	// Debug stuff
	case '`':
		if (!debug_mode) {
			(*pc)++; break;
		}
		switch (prgm[*pc+1]) {
		// Breakpoint
		case 'b':
			printf("\nSpice: %d - Seed: %lu - Position: %lu\n",
				   spice, bogus_seed, *pc);
			bogus_prompt_one();
			putchar('\n');
			break;
		// Display stack top
		case 'd':
			printf("%d\n", stack[stackptr]);
			break;
		// Display secondary stack top
		case 'e':
			printf("%d\n", stackb[stackbptr]);
			break;
		// Display stack truth value
		case 'p':
			stack[stackptr] > 0 ? printf("True\n") : printf("False\n");
			break;
		// Display secondary stack truth value
		case 'q':
			stackb[stackbptr] > 0 ? printf("True\n") : printf("False\n");
			break;
		// Wait for input before discarding it
		case 'i':
			getchar();
			break;
		}
		(*pc)++;
		break;

	case ' ': case '\t': case '\r': case '\n': break;
	default:
		if ((is >= 'A' && is <= 'Z') ||
			(is >= 'a' && is <= 'z') ||
			(is >= '0' && is <= '9')) {
			if (prgm[*pc+1] == '(') {
				functions[is] = *pc+1;
				*pc = bogus_skip_codeblock(prgm, *pc+1, prgm_len);
			}
			else bogus_do_codeblock(prgm, functions[is], prgm_len);
		} else
			fdie(*pc, "Unexpected character.");
		break;
	}

}

size_t bogus_do_codeblock(const char* prgm, size_t pc, size_t prgm_len) {
	if (prgm[pc] != '(')
		fdie(pc, "Expected paranthese.");
	pc++;
	
	while (pc < prgm_len && prgm[pc] != ')') {
		bogus_eval_one(prgm, &pc, prgm_len);
		pc++;
	}
	if (pc >= prgm_len) fdie(pc, "Unexpected EOF.");

	return pc;
}
size_t bogus_skip_codeblock(const char* prgm, size_t pc, size_t prgm_len) {
	I b = 1;
	for (pc += 1; b > 0 && pc < prgm_len; ++pc)
		switch (prgm[pc]) {
		case '(': b++; break;
		case ')': b--; break;
		case ';':
			for (++(pc); prgm[pc] != '\n' && pc < prgm_len; ++(pc));
			break;
		}
	if (pc > prgm_len) fdie(pc, "Unexpected EOF.");
	return pc-1;
}

void bogus_eval(const char* prgm) {
	size_t pc = 0;
	size_t prgm_len = strlen(prgm);
	char is;
	
	while (pc < prgm_len) {
		bogus_eval_one(prgm, &pc, prgm_len);
		++pc;
	}
}

/*********************************
 * MAIN PROGRAM
 */

void bogus_eval_file(const char* filename) {
	FILE* f = fopen(filename, "r");
	if (!f) die("fopen");

	if (fseek(f, 0, SEEK_END) == -1)
	die("fseek");
	size_t size = ftell(f);
	rewind(f);

	char* program = (char*)malloc(sizeof(char) * (size+1));
	for (size_t i = 0; i < size; ++i) {
		program[i] = fgetc(f);
		if (program[i] == EOF)
			pdie("inconsistent file size.");
	}
	program[size] = 0;

	bogus_eval(program);
	free(program);
}
bool bogus_prompt_one() {
	bool state = true;
	char* cmd = readline("bogus% ");
    
    if (!cmd) exit(0);
    else if (cmd[0] == ':') {
		if (!strcmp(cmd+1, "exit")) state = false;
		else if (!strcmp(cmd+1, "stack"))
			for (int i = stackptr-1; i > stackptr-10
						&& i >= 0; --i)
				printf("%d\n", stack[i]);
	} else
		bogus_eval(cmd);

	putchar('\n');
	free(cmd);

	return state;
}
void bogus_prompt() {
	printf("Bogus interpreter by matthilde.\n"
		   "Good luck figuring out how to write useful programs!\n"
		   "NOTE: functions doesn't work in the repl. too lazy.\n");

	bool state = true;
	while (state) {
		state = bogus_prompt_one();
	}
}

int main(int argc, char** argv) {
	// Generate random seed (very important)
	bogus_seed = time(NULL);
	srand(bogus_seed);
	bogus_init();

	if (argc == 1)
	    bogus_prompt();
	else {
		char* filename = NULL;
		int curarg;

		for (curarg = 1; curarg < argc; ++curarg) {
			// For some reason, I like reinventing the wheel.
			// why using getopt when you can just spin your own impl
			// :^)
			if (strequ(argv[curarg], "-d") && !debug_mode)
				debug_mode = true;
			else if (!filename)
				filename = argv[curarg];
			else {
				filename = NULL;
				curarg = argc;
			}
		}
		if (!filename) {
			fprintf(stderr, "USAGE: bogus [[-d] FILENAME]\n");
			return 1;
		}

		bogus_eval_file(filename);
	}

/*
	switch (argc) {
	case 1: bogus_prompt(); break;
	case 2: bogus_eval_file(argv[1]); break;
	default:
		break;
	}
*/
	return 0;
}
